from ssl import VerifyFlags
import face_recognition as fr
import cv2
import numpy as np
import os
from urllib.request import urlopen

VERIFY_DIST = 0.54

# define face comparing function
def face_verify(Image1, Image2):
    try:
        req1 = urlopen(Image1)
        arr1 = np.asarray(bytearray(req1.read()), dtype=np.uint8)
        image1 = cv2.imdecode(arr1, 1) # 'Load it as it is'


        req2 = urlopen(Image2)
        arr2 = np.asarray(bytearray(req2.read()), dtype=np.uint8)
        image2 = cv2.imdecode(arr2, 1) # 'Load it as it is'

        image1_encodeing = fr.face_encodings(image1)[0]


        unknown_face_encoding = fr.face_encodings(image2)[0]


        # results = fr.compare_faces([image1_encodeing], unknown_face_encoding)

        face_distances = fr.face_distance([image1_encodeing], unknown_face_encoding)


        dist = face_distances[0]
        print("*" * 30)
        print(dist)
        print("*" * 30)


        # return results[0]
        if dist < VERIFY_DIST:
            return True
        else:
            return False
    except:
        return False
