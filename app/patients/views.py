"""
Views for the patients APIs
"""
from distutils.command.upload import upload
from rest_framework import filters
from .compare import face_verify
from user.compare import faces_to_index

from typing import Generic
from rest_framework import (
    viewsets,
    mixins,
    status,
    generics,

)
import os
from django.conf import settings
from rest_framework.generics import GenericAPIView,CreateAPIView

from drf_spectacular.utils import (
    extend_schema_view,
    extend_schema,
    OpenApiParameter,
    OpenApiTypes,
)

from rest_framework.decorators import action,parser_classes
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser,FormParser

from core.models import (
    Image_File,
    Patients,
    Tag,
    Treatment,
)
from patients import serializers

@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'tags',
                OpenApiTypes.STR,
                description='Comma separated list of tag IDs to filter',
            ),
            OpenApiParameter(
                'treatments',
                OpenApiTypes.STR,
                description='Comma separated list of treatment IDs to filter',
            ),
        ]
    )
)


class AllPatientsViewSet(viewsets.ModelViewSet):
    """View for All manage patients APIs."""
    serializer_class = serializers.AllPatientSerializer
    queryset = Patients.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = [ 'id','first_name','last_name']


    def get_serializer_class(self):
        if self.action == "face_verify" :
          return serializers.VerifyImageSerializer
        return serializers.PatientsSerializer

    def _params_to_ints(self, qs):
        """Convert a list of strings to integers."""
        return [int(str_id) for str_id in qs.split(',')]






    @action(methods = ['GET'],detail = True,url_path = 'get_images')
    def get_images(self,request,*args, **kwargs):
        patients = self.get_object()
        serializer = serializers.PatientsImageListSerializer(patients,data = request.data)
        if serializer.is_valid():
            context = serializer.data
            return Response(context,status = status.HTTP_201_CREATED)


    @action(methods=['POST'], detail=True, url_path='faceverify')
    @parser_classes([FormParser])
    def face_verify(self, request, pk= None):

        patients = self.get_object()
        context = request.data
        flag = False
        serializer = self.get_serializer(patients,data = request.data)

        if serializer.is_valid():
            serializer.save()
            context = serializer.data
            upload_image = context["image"]

            image_serializer = serializers.PatientsImageListSerializer(patients,data = request.data)
            if image_serializer.is_valid():
                cnt = 0
                tot_cnt = 0
                lists = image_serializer.data
                urls = []
                urls.append(upload_image)
                for item in lists["image_lists"]:

                    urls.append(item["image"])
                    print(item["image"])
                    print(upload_image)
                    tot_cnt = tot_cnt + 1

                    imagepath = ""
                    if not os.environ.get('AWS_EXECUTION_ENV'):
                      imagepath = settings.BASE_URL  +item["image"]
                    else :
                        imagepath = item["image"]

                    flag = face_verify(imagepath,upload_image)

                    if flag == True:
                        cnt = cnt + 1
                    if cnt > 2 :
                        break
            if cnt > 0:
                flag = True
            else: flag = False

            if tot_cnt == 0:
                return Response({"status" : False,"accuracy": urls},status = status.HTTP_400_BAD_REQUEST)
            return Response({"status" : flag, "accuracy" : urls},status = status.HTTP_201_CREATED)

        return Response({"status" : False,"accuracy": urls},status = status.HTTP_400_BAD_REQUEST)


class PatientsViewSet(viewsets.ModelViewSet):
    """View for manage patients APIs."""
    serializer_class = serializers.PatientsDetailSerializer
    queryset = Patients.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = [ 'first_name','last_name']



    def _params_to_ints(self, qs):
        """Convert a list of strings to integers."""
        return [int(str_id) for str_id in qs.split(',')]

    def get_queryset(self):
        """Retrieve recipatientspes for authenticated user."""
        tags = self.request.query_params.get('tags')
        treatment = self.request.query_params.get('treatment')
        queryset = self.queryset
        if tags:
            tag_ids = self._params_to_ints(tags)
            queryset = queryset.filter(tags__id__in=tag_ids)
        if treatment:
            treatmen_ids = self._params_to_ints(treatment)
            queryset = queryset.filter(treatment__id__in=treatmen_ids)

        return queryset.filter(
            user=self.request.user
        ).order_by('-id').distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
          return serializers.PatientsSerializer
        elif self.action == 'upload_image':
          return serializers.PatientsImageListSerializer
        elif self.action == "face_verify" :
            return serializers.VerifyImageSerializer

        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new patients."""
        serializer.save(user=self.request.user)


    @action(methods=['POST'], detail=True, url_path='upload-image')
    def upload_image(self, request, *args, **kwargs):
        """Upload an image to patients."""

        files = request.FILES.getlist('image_lists')
        uploaded_images = []
        if files:
            request.data.pop('image_lists')
        for file in files:
               content = Image_File.objects.create(user=self.request.user ,image=file)
               uploaded_images.append(content)

        patient_id = None
        for key, val in request.data.items():
            if key == 'id':
                patient_id = val
        for item in uploaded_images:
            faces_to_index(item.image, patient_id)

        patients = self.get_object()
        serializer = serializers.PatientsImageListSerializer(patients,data = request.data)
        if serializer.is_valid():
            patients.image_lists.add(*uploaded_images)
            patients.save()
            context = serializer.data
            return Response(context,status = status.HTTP_201_CREATED )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=True, url_path='faceverify')
    @parser_classes([FormParser])
    def face_verify(self, request, pk= None):

        patients = self.get_object()
        context = request.data
        flag = False
        serializer = self.get_serializer(patients,data = request.data)

        if serializer.is_valid():
            serializer.save()
            context = serializer.data
            upload_image = context["image"]

            image_serializer = serializers.PatientsImageListSerializer(patients,data = request.data)
            if image_serializer.is_valid():
                cnt = 0
                tot_cnt = 0
                lists = image_serializer.data
                for item in lists["image_lists"]:

                    print(item["image"])
                    tot_cnt = tot_cnt + 1
                    imagepath = settings.BASE_URL  +item["image"]

                    flag = face_verify(imagepath,upload_image)


                    if flag == True:
                        cnt = cnt + 1
                    if cnt > 2 :
                        break
            if cnt > 0:
                flag = True
            else: flag = False

            if tot_cnt == 0:
                return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)
            return Response({"status" : flag, "accuracy" : cnt / tot_cnt },status = status.HTTP_201_CREATED)

        return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)

@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'assigned_only',
                OpenApiTypes.INT, enum=[0, 1],
                description='Filter by items assigned to patients.',
            ),
        ]
    )
)

class BasePatientsAttrViewSet(mixins.DestroyModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.ListModelMixin,
                              viewsets.GenericViewSet):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        assigned_only = bool(
            int(self.request.query_params.get('assigned_only', 0))
        )
        queryset = self.queryset
        if assigned_only:
            queryset = queryset.filter(patients__isnull=False)

        return queryset.filter(
            user=self.request.user
        ).order_by('-name').distinct()


class TagViewSet(BasePatientsAttrViewSet):
    """Manage tags in the database."""
    serializer_class = serializers.TagSerializer
    queryset = Tag.objects.all()


class TreatmentViewSet(BasePatientsAttrViewSet):

    """Manage treatment in the database."""
    serializer_class = serializers.TreatmentSerializer
    queryset = Treatment.objects.all()
