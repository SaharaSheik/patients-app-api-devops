from django.contrib.auth import (
    get_user_model,
    authenticate,
)
from rest_framework import serializers

from core.models import (
    Image_File,
    Patients,
    Tag,
    Treatment,
    User
)
from rest_framework.response import Response

class AdminSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['email',
                  'password',
                  ]
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}

    def create(self, validated_data):
        """Create and return a admin with encrypted password."""
        get_user_model().objects.create_superuser(**validated_data)
        users = get_user_model().objects.filter(email = validated_data["email"])
        user = users[0]
        user.save()
        return user



class ResetPasswordByAdminSerializer(serializers.Serializer):

    model = get_user_model()
    id = serializers.IntegerField(required = True)
    password = serializers.CharField(required=True)

    def save(self, data):
      userid = data["id"]
      password = data['password']
      users = get_user_model().objects.filter(id = userid)
      user = users[0]
      if user.is_superuser == True:
                return Response({"status":False, "error" : "SuperUser cannot be modified"})

      user.set_password(password)
      user.save()
      return Response({'status' : 'true'})


class SetActivateUserSerializer(serializers.Serializer):
        model = get_user_model()
        id = serializers.IntegerField(required = True)
        isActive = serializers.BooleanField(default = False)

        def save(self, data):
            userid = data["id"]
            isActive = data['isActive']
            users = get_user_model().objects.filter(id = userid)
            user = users[0]
            if user.is_superuser == True:
                return Response({"status":False, "error" : "SuperUser cannot be modified"})
            if isActive == "true" :
                  user.is_active = True
            else : user.is_active = False
            user.save()
            return Response({ 'userid' : userid, 'isActive' : isActive,"status" : True})


class UserFaceCompareSerializer(serializers.Serializer):

    image1 = serializers.ImageField(required = True)





class UserFaceVerifySerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ['id', 'image']
        read_only_fields = ['id']