#from facepplib import FacePP ,exceptions
from  user.preprocess_image import preprocess_image
from  user.model_tools import FacialRecognitionModel
import numpy as np
import os
from urllib.request import urlopen
import cv2
from django.conf import settings

# define face comparing function
def face_comparing(Image1):
    try:
        model = FacialRecognitionModel()
        model.load_user_index()

        image = preprocess_image(Image1)

        prediction = model.lookup_image(image)

        predict_id = prediction["Rank 1"]

        return predict_id
    except:
        return -1





def faces_to_index(Image1, id):
    model = FacialRecognitionModel()
    model.load_user_index()

    image_list = []
    label_list = []
    try:
        image = preprocess_image(Image1)
    except:
        raise ValueError(f'Could not add Image because of some reason\n{Image1}' )
    id = int(id)

    model.add_to_index(image, id)
    model.get_index_summary()
