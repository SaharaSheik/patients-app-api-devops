"""
URL mappings for the user API.
"""
from django.urls import (
    path,
    include,
)

from rest_framework.routers import DefaultRouter

from admins import views
router = DefaultRouter()


router.register('patients',views.PatientsViewSet)
router.register('users',views.UserViewSet)
app_name = 'admins'

urlpatterns = [
    path('create/', views.CreateAdminView.as_view(), name='create'),
    path('', include(router.urls)) ,
    path('user/resetpwd/',views.ResetPasswordView.as_view(),name = 'resetpwd'),
    path('user/setActive/',views.SetActiveUserView.as_view(),name = 'setActive'),
    path('user/face_recognize/',views.FaceCompareView.as_view(),name = "user_face_recognize")
]
