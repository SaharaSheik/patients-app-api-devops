
"""
Views for the user API.
"""

from .compare import face_comparing,faces_to_index
from patients.compare import face_verify
from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view


from django.core.files.storage import FileSystemStorage

from django.core.exceptions import ValidationError
from rest_framework.decorators import action,parser_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import MultiPartParser,FormParser

from django.contrib.auth.tokens import default_token_generator
from django.template.loader import render_to_string

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated,IsAdminUser
from rest_framework import filters

from django.contrib.auth import (
    get_user_model,
    authenticate,
)
from django.conf import settings

from admins.serializers import (
    AdminSerializer,
    ResetPasswordByAdminSerializer,
    SetActivateUserSerializer,
    UserFaceCompareSerializer,
    UserFaceVerifySerializer
)

from rest_framework import (
    viewsets,
    mixins,
    status,
    generics,

)

from patients.serializers import (
    AllPatientSerializer,
    PatientsSerializer,
)

from user.serializers import (
    UserSerializer,
    UserImageListSerializer

)

from core.models import (
    Image_File,
    Patients,
    Tag,
    Treatment,
    User
)


from admins.Permission import (
    UserPermission,
)

def checkIsAdmin(user):
    return user.is_superuser

class CreateAdminView(generics.CreateAPIView):
    """Create a new Admin in the system."""
    serializer_class = AdminSerializer



class PatientsViewSet(viewsets.ModelViewSet):
    """View for All manage patients APIs."""


    serializer_class = AllPatientSerializer
    queryset = Patients.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = (UserPermission,)
    filter_backends = [filters.SearchFilter]
    search_fields = [ 'id','first_name','last_name']



    def create(self, request, *args, **kwargs):
        serializer = AllPatientSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def update(self, request, *args, **kwargs):
        serializer = AllPatientSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)



class UserViewSet(viewsets.ModelViewSet):
    """View for All manage Users APIs."""

    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = (UserPermission,)
    filter_backends = [filters.SearchFilter]
    search_fields = [ 'id','email','first_name','last_name']


    def get_serializer_class(self):

        if self.action == "face_verify" :
            return UserFaceVerifySerializer
        return self.serializer_class

    @action(methods = ['GET'],detail = True,url_path = 'get_images')
    def get_images(self,request,*args, **kwargs):
        patients = self.get_object()
        serializer = UserImageListSerializer(patients,data = request.data)
        if serializer.is_valid():
            context = serializer.data
            return Response(context,status = status.HTTP_201_CREATED)

    @action(methods=['POST'], detail=True, url_path='faceverify')
    @parser_classes([FormParser])
    def face_verify(self, request, pk= None):

        users = self.get_object()
        context = request.data
        flag = False
        serializer = self.get_serializer(users,data = request.data)

        if serializer.is_valid():
            serializer.save()
            context = serializer.data
            upload_image = context["image"]

            image_serializer = UserImageListSerializer(users,data = request.data)
            if image_serializer.is_valid():
                cnt = 0
                tot_cnt = 0
                lists = image_serializer.data
                for item in lists["image_lists"]:

                    print(item["image"])
                    tot_cnt = tot_cnt + 1
                    imagepath = settings.BASE_URL  +item["image"]

                    flag = face_verify(imagepath,upload_image)


                    if flag == True:
                        cnt = cnt + 1
                    if cnt > 2 :
                        break
            if cnt > 0:
                flag = True
            else: flag = False

            if tot_cnt == 0:
                return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)
            return Response({"status" : flag, "accuracy" : cnt / tot_cnt },status = status.HTTP_201_CREATED)

        return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)




class ResetPasswordView(generics.UpdateAPIView):
    """Reset Password"""
    serializer_class = ResetPasswordByAdminSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated&IsAdminUser]
    def update(self,request):

        serializer = self.get_serializer(data=request.data)
        return  serializer.save(request.data)





class SetActiveUserView(generics.UpdateAPIView):
    """Active user"""
    serializer_class = SetActivateUserSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated&IsAdminUser]

    def update(self,request):
        serializer = self.get_serializer(data=request.data)
        return serializer.save(request.data)





class FaceCompareView(generics.CreateAPIView):

    serializer_class = UserFaceCompareSerializer

    @parser_classes([FormParser])
    def create(self, request):


        upload = request.FILES['image1']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url =  settings.BASE_URL  + fss.url(file)

        cp = face_comparing(file_url)

        if(str(cp).isdigit()):
            patients = get_user_model().objects.filter(id = cp)
            if(len(patients) < 1) :
                return Response({"T" : "Not Found", "url" : file_url})

        return Response({"T": cp, "url":file_url}, status=status.HTTP_201_CREATED)
