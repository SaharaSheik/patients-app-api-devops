"""
Views for the user API.
"""
from patients.compare import face_verify
from .compare import face_comparing,faces_to_index,faces_to_user_index
from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from django.core.files.storage import FileSystemStorage

from rest_framework.decorators import action,parser_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import MultiPartParser,FormParser

from base64 import urlsafe_b64decode, urlsafe_b64encode
from django.contrib.auth.tokens import default_token_generator
from django.template.loader import render_to_string
from threading import Thread

from django.contrib.auth import (
    get_user_model,
    authenticate,
)
from django.conf import settings
from .mail_verify import get_email_token, get_random_string, send_mail,verify_token,verify_key
from sendgrid.helpers.mail import Mail

from user.serializers import (
    DigitKeyVerifySerializer,
    EmailVerifiySerializer,
    FaceCompareTestSerializer,
    UserSerializer,
    AuthTokenSerializer,
    ChangePasswordSerializer,
    ResetPasswordSerializer,
    UserImageListSerializer,
    UserImageSerializer,
    VerifyImageSerializer
)

from core.models import (
    Image_File,
    Patients
)
class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system."""
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user."""
    serializer_class = AuthTokenSerializer
    # added colorfull api views
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user."""
    serializer_class = UserSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        """Retrieve and return the authenticated user."""
        return self.request.user

class GetUserImagesView(generics.ListAPIView):

    serializer_class = UserImageListSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        """Retrieve and return the authenticated user."""
        return self.request.user
    def get(self,request):
        users = self.get_object()
        serializer = UserImageListSerializer(users,data = request.data)
        if serializer.is_valid():
            context = serializer.data
            return Response(context,status = status.HTTP_201_CREATED)


class UploadImageView(generics.CreateAPIView):

    serializer_class = UserImageListSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]


    def get_object(self):
        """Retrieve and return the authenticated user."""
        return self.request.user

    @parser_classes([FormParser])
    def create(self, request):
        """Upload  images to Users."""

        files = request.FILES.getlist('image_lists')
        uploaded_images = []
        if files:
            request.data.pop('image_lists')
        for file in files:
               content = Image_File.objects.create(user=self.request.user ,image=file)
               uploaded_images.append(content)

        user = self.get_object()

        print(user.id)
        for item in uploaded_images:
            faces_to_user_index(item.image, user.id)

        serializer = UserImageListSerializer(user,data = request.data)
        if serializer.is_valid():
            print(user)
            user.image_lists.add(*uploaded_images)
            user.save()
            context = serializer.data
            return Response(context,status = status.HTTP_201_CREATED )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class FaceVerifyView(generics.CreateAPIView):

    serializer_class = UserImageSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]


    def get_object(self):
        """Retrieve and return the authenticated user."""
        return self.request.user

    @parser_classes([FormParser])
    def create(self, request):
        """Verify Face"""
        user = self.get_object()
        context = request.data
        flag = False
        serializer = VerifyImageSerializer(user,data = request.data)
        cnt = 0
        tot_cnt = 0

        if serializer.is_valid():
            serializer.save()
            context = serializer.data
            upload_image = context["image"]

            image_serializer = UserImageSerializer(user,data = request.data)
            if image_serializer.is_valid():

                lists = image_serializer.data
                for item in lists["image_lists"]:

                    print(item["image"])
                    tot_cnt = tot_cnt + 1
                    imagepath = settings.BASE_URL  +item["image"]

                    flag = face_verify(imagepath,upload_image)

                    if flag == True:
                        cnt = cnt + 1
                    if cnt > 2 :
                        break
            if cnt > 0:
                flag = True
            else: flag = False

            if tot_cnt == 0:
                return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)
            return Response({"status" : flag, "accuracy" : cnt / tot_cnt },status = status.HTTP_201_CREATED)

        return Response({"status" : False,"accuracy": 0},status = status.HTTP_400_BAD_REQUEST)







class ChangePasswordView(generics.UpdateAPIView):
    """An endpoint for changing password"""
    serializer_class = ChangePasswordSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    def get_object(self,queryset = None):
        return self.request.user
    def update(self,request,*args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

        return Response(response)

class VerifySendEmailView(generics.CreateAPIView):
    serializer_class = EmailVerifiySerializer
    def create(self,request):
        email= request.data['email']

        users = get_user_model().objects.filter(email = email)
        random_key = get_random_string(6)
        if  users.count() == 0 :
            return Response({"user" : "The user not sign up" ,"status" : False})

        else :
            from_Email = settings.EMAIL_FROM

            data = {
                "email" : email,
                "verify_key" : random_key
            }

            serializer = self.get_serializer(data=data)
            user_ = serializer.save(data)

            message = Mail(
            from_email= from_Email,
            to_emails= email,
            subject='This is email verification for Rosro Application',
            html_content = f'<div> <h1>{random_key}</h1> This is your Authentication code </div> <div> Please Do not share it with anyone.</div>'
            )
            send_mail(message)
            return Response({"key" :random_key ,"status" : True},status = status.HTTP_201_CREATED)


class ResetSendEmailView(generics.CreateAPIView):
    serializer_class = EmailVerifiySerializer
    def create(self,request):
        email= request.data['email']

        users = get_user_model().objects.filter(email = email)
        random_key = get_random_string(6)
        if  users.count() == 0 :
            return Response({"user" : "The user not sign up" ,"status" : False})

        else :
            from_Email = settings.EMAIL_FROM

            data = {
                "email" : email,
                "verify_key" : random_key
            }

            serializer = self.get_serializer(data=data)
            user_ = serializer.save(data)

            message = Mail(
            from_email= from_Email,
            to_emails= email,
            subject='This is Reset Password Code for Rosro Application',
            html_content = f'<div> <h1>{random_key}</h1> This is your Reset Password Check code </div> <div> Please confirm this and use this code to reset password.</div>'
            )
            send_mail(message)
            return Response({"key" :random_key ,"status" : True},status = status.HTTP_201_CREATED)




class KeyVerifyView(generics.CreateAPIView):
    serializer_class = DigitKeyVerifySerializer

    def create(self,request):

        email= request.data['email']
        key = request.data['key']
        data = {
            "email" : email,
            "verify_key" : key
        }
        flag = verify_key(data)
        return Response({"status" : flag}, status=status.HTTP_201_CREATED)

# check receive email_token and activate user


@api_view(['GET'])
def activate(request, email_token):
    flag = verify_token(email_token)
    if flag:
        return Response({"message":
                        'Thanks for email confirmation. You can login.',
                         "status": True}, status=status.HTTP_201_CREATED)
    else:
        return Response({"message": 'Activation link is invalid!',
                        "status": False})


class ResetPasswordView(generics.UpdateAPIView):
    """Reset Password"""
    serializer_class = ResetPasswordSerializer

    def update(self,request,*args, **kwargs):
        email= request.data['email']

        users = get_user_model().objects.filter(email = email)
        if  users.count() == 0 :
            return Response({"user" : "not user found" ,"status" : False})


        user = users[0]

        if(user.verify_key != request.data['key']):
            return Response({"key" : "check string not correct" ,"status" : False})

        password = request.data['password']
        user.set_password(password)
        user.save()

        return Response({"status" : True})


class FaceCompareView(generics.CreateAPIView):

    serializer_class = FaceCompareTestSerializer

    @parser_classes([FormParser])
    def create(self, request):


        upload = request.FILES['image1']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url =  settings.BASE_URL  + fss.url(file)

        cp = face_comparing(file_url)

        if(str(cp).isdigit()):
            patients = Patients.objects.filter(id = cp)
            if(len(patients) < 1) :
                return Response({"T" : "Not Found", "url" : file_url})

        return Response({"T": cp, "url":file_url}, status=status.HTTP_201_CREATED)
