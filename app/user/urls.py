"""
URL mappings for the user API.
"""
from django.urls import path

from user import views
from .views import activate

app_name = 'user'

urlpatterns = [
    path('create/', views.CreateUserView.as_view(), name='create'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('me/', views.ManageUserView.as_view(), name='me'),

    path('changepwd/',views.ChangePasswordView.as_view(),name = "changepwd"),
    path('send_verifyemail/',views.VerifySendEmailView.as_view(),name = "emailverify"),
    path('send_resetpwdemail/',views.ResetSendEmailView.as_view(),name = "emailresetpassword"),
    path('email/verify/',views.KeyVerifyView.as_view(), name = "activate"),
    path('faceCompare/',views.FaceCompareView.as_view(),name = "patient_face_compare"),
    path('faceverify/', views.FaceVerifyView.as_view(),name = "user_face_verify"),
    path('resetpwd/',views.ResetPasswordView.as_view(),name = "resetpwd"),
    path('upload-image/',views.UploadImageView.as_view(),name = "user_upload-image"),
    path('get_selfimages/',views.GetUserImagesView.as_view(),name = "getselfimages")
    ]
