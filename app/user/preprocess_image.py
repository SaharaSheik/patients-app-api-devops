import numpy as np
import cv2
from skimage import io
from io import BytesIO
from deepface.commons import functions as df_fn


def preprocess_image(file_name):
  input_shape = (96, 96)
  image = io.imread(file_name)
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

  image = df_fn.preprocess_face(image, target_size=input_shape , detector_backend='opencv'
  )



  return image[0]
