import tensorflow as tf
from tensorflow_similarity.models import SimilarityModel as sim
# import numpy as np
# import os
from tensorflow.keras import backend as K
from tensorflow import keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import (
                                       Conv2D,
                                       ZeroPadding2D,
                                       Activation, Input,
                                       concatenate )
from tensorflow.keras.layers import (
                                       Dense, Activation,
                                       Lambda, Flatten,
                                       BatchNormalization )
from tensorflow.keras.layers import MaxPooling2D, AveragePooling2D
from tensorflow.keras.models import load_model

from deepface.basemodels.OpenFace import loadModel
#

THRESOLD_VALUE = 0.65

class FacialRecognitionModel():

    def __init__(self):
        model_path = '/app/user/models/my_model'
        self.model = tf.keras.models.load_model(
                                                model_path,
                                                custom_objects={
                                                    "SimilarityModel": sim}
                                                 )

    def add_to_index(self, image, ids):
        self.load_index()
        # raise ValueError (f"The shape is {np.asarray(images).shape}")
        self.model.index_single(image, ids, data=image, build=True)
        self._save_index()

    def add_to_user_index(self, image, ids):
        self.load_index()
        # raise ValueError (f"The shape is {np.asarray(images).shape}")
        self.model.index_single(image, ids, data=image, build=True)
        self._save_user_index()

    def load_index(self):
        index_path = r'/app/user/face_indexs/base_index'
        self.model.reset_index()
        self.model.load_index(index_path)

    def load_user_index(self):
        index_path = r'/app/admins/face_indexs/base_index'
        self.model.reset_index()
        self.model.load_index(index_path)


    def _save_index(self):
        index_path = r'/app/user/face_indexs/base_index'
        self.model.save_index(index_path)
        self.model.reset_index()

    def _save_user_index(self):
        index_path = r'/app/admins/face_indexs/base_index'
        self.model.save_index(index_path)
        self.model.reset_index()

    def lookup_image(self, image):
        lookup_n = 10
        prediction = self.model.single_lookup(image, k=lookup_n)

        print(prediction[0].distance)

        if len(prediction) == 0 :
            rank = {"Rank " + str(i + 1): "None" for i in range(lookup_n)}
        elif prediction[0].distance > THRESOLD_VALUE:
              rank = {"Rank " + str(i + 1): "Not Found" for i in range(lookup_n)}
        else :
            rank = {"Rank " + str(i + 1):
                prediction[i].label for i in range(lookup_n)}
        return rank

    def get_index_summary(self):
        return self.model.index_summary
