import os

from django.contrib.auth.tokens import default_token_generator
from base64 import urlsafe_b64decode, urlsafe_b64encode

from django.contrib.auth import (
    get_user_model,
)

import json
import random
from django.conf import settings
import datetime,time
import string
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

def get_random_string(length):
    #choose lower letters
    result_str = ''.join(str(random.randint(0,9)) for i in range(length))
    return result_str

def get_email_token(email = None) :
    s_time = time.time()
    send_data = {"email" : email,"expired" : str(s_time)}
    data = json.dumps(send_data)

    confirmation_token = urlsafe_b64encode((data).encode('utf-8'))
    token = confirmation_token.decode("utf-8")
    return token

def send_mail(message):
    sg = SendGridAPIClient(settings.SEND_GRID_KEY)
    sg.send(message)
  

def verify_key(data):
    email = data['email']
    key = data['verify_key']
    try:
        users = get_user_model().objects.filter(email = email)
        for user in users:
             if(user.verify_key == key):
                    user.is_active = True
                    user.save()
                    return True
             else: return False
    except BaseException:
            pass
    return False

def verify_token(email_token):
        data = urlsafe_b64decode(email_token).decode("utf-8")
        data = json.loads(data)
        email = data['email']
        sended_time = (float)(data['expired'])

        current_time = time.time()
        delta_time = current_time - sended_time


        if(delta_time > settings.EXPIRE_TIME):
                 return False
        try:
            users = get_user_model().objects.filter(email = email)
            for user in users:
                    user.is_active = True
                    user.save()
                    return True
        except BaseException:
            pass
        return False
