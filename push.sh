#!/bin/bash
#
# Sample script to push test image
# Expects AWS credentials and region to be in the environment
#

REPO=595592471409.dkr.ecr.us-east-1.amazonaws.com
IMAGE=patients-app-api-devops
TAG=test

docker build . -t ${REPO}/${IMAGE}:${TAG} --compress

aws ecr get-login-password | docker login --username AWS --password-stdin ${REPO}

docker push ${REPO}/${IMAGE}:${TAG}
