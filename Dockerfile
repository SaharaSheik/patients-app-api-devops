FROM python:3.7.15-buster
LABEL maintainer="Sahara"

# Environment and args
ENV PYTHONUNBUFFERED 1
ENV NUMBA_CACHE_DIR=/tmp/numba_cache
ARG DEV=false
WORKDIR /app

# Copy python requirements
COPY ./requirements.txt /tmp/requirements.txt
COPY ./requirements.dev.txt /tmp/requirements.dev.txt

# Install dependencies
RUN apt update && \
    pip install --upgrade pip && \
    apt install -y postgresql-client libjpeg-dev gcc libc-dev \
        linux-headers-5.10 libpq-dev musl-dev build-essential \
        musl-dev zlib1g zlib1g-dev python3-opencv && \
    pip install --no-binary :all: nmslib && \
    pip install CMake  && \
    pip install dlib && \
    pip install -r /tmp/requirements.txt && \
    if [[ $DEV = "true" ]]; then pip install -r /tmp/requirements.dev.txt; fi && \
    apt-get remove -y gcc linux-headers-5.10 build-essential && \
    rm -rf /tmp

# Copy app and scripts
COPY ./app /app
COPY ./scripts/ /scripts/

# Create working directories
RUN mkdir -p /vol/web/media && \
    mkdir -p /vol/web/static && \
    mkdir -p ${NUMBA_CACHE_DIR}

# Fix permissions
RUN chmod -R 755 /scripts/ && \
    chmod -R 755 /vol && \
    chmod 777 ${NUMBA_CACHE_DIR}

# Create Django user and fix permissions
RUN adduser --disabled-password --no-create-home django-user && \
    chown -R django-user:django-user /vol && \
    chown -R django-user:django-user ${NUMBA_CACHE_DIR} && \
    chown -R django-user:django-user /app

ENV PATH="/scripts:$PATH"
EXPOSE 8000
USER django-user
VOLUME /vol/web
CMD [ "entrypoint.sh"]

# FROM python:3.7.15-buster
# # FROM python:3.9
# LABEL maintainer="Sahara"

# ENV PYTHONUNBUFFERED 1
# ENV NUMBA_CACHE_DIR=/tmp/numba_cache
# ENV PATH="/scripts:${PATH}"

# RUN pip install --upgrade pip
# RUN pip install --no-binary :all: nmslib
# RUN pip install CMake
# RUN pip install dlib
# RUN apt-get update
# RUN apt-get update && apt-get install -y python3-opencv

# COPY ./requirements.txt /tmp/requirements.txt
# COPY ./requirements.dev.txt /tmp/requirements.dev.txt
# COPY ./app /app
# COPY ./scripts/ /scripts/
# RUN chmod +x /scripts/*
# WORKDIR /app
# EXPOSE 8000

# ARG DEV=false
# RUN python -m venv /py && \
#     /py/bin/pip install --upgrade pip && \
#     apt update  && \
#     apt install -y postgresql-client libjpeg-dev && \
#     apt install -y \
#         gcc libc-dev linux-headers-5.10 libpq-dev musl-dev \
#         build-essential musl-dev zlib1g zlib1g-dev && \
#     /py/bin/pip install -r /tmp/requirements.txt && \
#     if [ $DEV = "true" ]; \
#         then /py/bin/pip install -r /tmp/requirements.dev.txt ; \
#     fi && \
#     rm -rf /tmp && \
#     adduser \
#         --disabled-password \
#         --no-create-home \
#         django-user && \
#     mkdir -p /vol/web/media && \
#     mkdir -p /vol/web/static && \
#     chown -R django-user:django-user /vol && \
#     chmod -R 755 /vol && \
#     mkdir -p ${NUMBA_CACHE_DIR} && \
#     chmod 777 ${NUMBA_CACHE_DIR} && \
#     chown -R django-user:django-user ${NUMBA_CACHE_DIR}

# ENV PATH="/py/bin:$PATH"

# USER django-user
# VOLUME /vol/web
# CMD [ "entrypoint.sh"]